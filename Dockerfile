FROM node:alpine

WORKDIR "/app"

COPY ./package.json ./

RUN npm install

COPY . .

RUN npm install --prefix client && npm run build --prefix client

CMD ["npm", "run", "start"]